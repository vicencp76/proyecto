function loginUsersV2(req, res) {
   console.log("POST /apitechu/v2/login");

     var query = "q=" + JSON.stringify({"email": req.body.email});
     console.log("query es " + query);

     httpClient = requestJson.createClient(baseMLabURL);
     httpClient.get("user?" + query + "&" + mLabAPIKey,
       function(err, resMLab, body) {

         var isPasswordcorrect =
           crypt.checkPassword(req.body.password, body[0].password);
         console.log("Password correct is " + isPasswordcorrect);

         console.log("isPasswordcorrect :" + isPasswordcorrect);

         if (!isPasswordcorrect) {
           var response = {
             "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
           }
           res.status(401);
           res.send(response);
         } else {
           console.log("Got a user with that email and password, logging in");
           var id = Number.parseInt(body[0].id);
           query = "q=" + JSON.stringify({"id": id});
           console.log("Query for put is " + query);
           var putBody = '{"$set":{"logged":true}}';
           httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
             function(errPUT, resMLabPUT, bodyPUT) {
               console.log("PUT done");
               var response = {
                 "msg" : "Usuario logado con éxito",
                 "idUsuario" : body[0].id
               }
               res.send(response);
             }
           )
         }
       }
     );
    }




    function logoutUsersV2(req, res) {
       console.log("POST /apitechu/v2/logout/:id");

       var id = Number.parseInt(req.params.id);
       var query = "q=" + JSON.stringify({"id": id});
       console.log("query es " + query);

       httpClient = requestJson.createClient(baseMLabURL);
       httpClient.get("user?" + query + "&" + mLabAPIKey,
         function(err, resMLab, body) {
           if (body.length == 0) {
             var response = {
               "mensaje" : "Logout incorrecto, usuario no encontrado"
             }
             res.send(response);
           } else {
             console.log("Got a user with that id, logging out");
             id = Number.parseInt(body[0].id);
             query = "q=" + JSON.stringify({"id": id});
             console.log("Query for put is " + query);
             var putBody = '{"$unset":{"logged":""}}'
             httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
               function(errPUT, resMLabPUT, bodyPUT) {
                 console.log("PUT done");
                 var response = {
                   "msg" : "Usuario deslogado",
                   "idUsuario" : body[0].id
                 }
                 res.send(response);
               }
             )
           }
         }
       );
      }
