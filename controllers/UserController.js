const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL =  "https://api.mlab.com/api/1/databases/apitechuvcp13ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1 (req, res){
  console.log("GET /apitechu/v1/users ");
//  console.log("$COUNT " + req.query.$count);
//  console.log("$TOP " + req.query.$top);

var user = require('../usuarios.json');
var userlen = user.length;

if (req.query.$top){
   user = user.slice(0,req.query.$top)
 }

if (req.query.$count){
    res.send({"user":user,"count":userlen});
 }
    res.send({"user":user});
}

function getUsersV2 (req, res){
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  httpClient.get("user?" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = !err ? body : {
      "msg" : "Error obteniendo usuarios."
    }
    res.send(response);
  }
);
}

function getUserByIdV2 (req, res){
  console.log("GET /apitechu/v2/users/:id");

  //var id = req.params.id;
  //console.log("La id del usuario a buscar es " + id);
  //var query = 'q={"id":' + id + '}';

  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a buscar es " + id);
  var query = "q=" + JSON.stringify({"id":id});
  console.log("query es " + query);

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
        res.send(response);
      } else {
        if (body.length > 0) {
          var response = body[0];
//          var response = {
//            "msg" : "Usuario encontrado"
//          }
          res.send(response);
          }
          else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
      }
 );
}

function createUserV1(req,res) {
  console.log("POST /apitechu/v1/users");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }

  console.log(newUser);

  var users = require('../usuarios.json');
  users.push(newUser);
  console.log("Usuario añadido al array");

  io.writeUserDataToFile(users);
  console.log("Proceso de creación de usuario finalizado");


}

function createUserV2(req,res) {
  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
// OJO quitar este console.log para securizarlo
  console.log(req.body.password);

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
}

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  httpClient.post("user?" + mLabAPIKey, newUser,
  function(err, resMLab, body) {
    console.log("Usuario creado en Mlab");
    res.status(201).send({"msg" : "Usuario creado"})
    }
  )
}

function deleteUserV1(req, res) {
   console.log("DELETE /apitechu/v1/users/:id");
   console.log("id del usuario a borrar es " + req.params.id);

   var users = require('../usuarios.json');
   var deleted = false;

    console.log("Usando for normal");
   for (var i = 0; i < users.length; i++) {
     console.log("comparando " + users[i].id + " y " +  req.params.id);
     if (users[i].id == req.params.id) {
       console.log("La posicion " + i + " coincide");
       users.splice(i, 1);
       deleted = true;
       break;
     }
   }

   io.writeUserDataToFile(users);
   console.log("Proceso de borrado de usuario finalizado");

   }

function deleteUserV2(req, res) {
  console.log("DELETE /apitechu/v2/users/:id");

  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a borrar es " + id);
  var query = "q=" + JSON.stringify({"id": id});
  console.log("query es " + query);

// [] OK
// [{}] OK
// {} NOK leaves document with _id, it's an update
// JSON.parse('[{}]') also OK
// JSON.parse('[]') also OK
  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.put("user?" + query + "&" + mLabAPIKey, [],
    function(err, resMLab, body) {
      var response = !err ? body : {
        "msg" : "Error borrando usuario."
      }
    res.send(response);
    }

);
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.deleteUserV2 = deleteUserV2;
