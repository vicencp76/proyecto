const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL =  "https://api.mlab.com/api/1/databases/apitechuvcp13ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginUsersV1 (req, res){
  console.log("LOGIN /apitechu/v1/login");

  var users = require('../usuarios.json');
  var login = false;
  var usuarioid = 0;

  console.log("Realizando login de usuario");

  for (var i = 0; i < users.length; i++) {
           if ((users[i].email == req.body.email) &&
           (users[i].password == req.body.password)){
               users[i].logged = true;
               usuarioid = users[i].id;
               login = true;

           }

        }
        if  (login == true)
          {var respuesta = {
              "mensaje":  "Login correcto",
              "idUsuario": usuarioid
          };

           io.writeUserDataToFile(users);
           console.log("Login de usuario persistido");
           res.send(respuesta);
          }
        else
          {res.send({"mensaje":"Login incorrecto"});
          }
     }

 function loginUsersV2(req, res) {
    console.log("POST /apitechu/v2/login");

      var query = "q=" + JSON.stringify({"email": req.body.email});

      httpClient = requestJson.createClient(baseMLabURL);
      httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          
      if (body.length > 0) {
         var controlpassword =
         crypt.checkPassword(req.body.password, body[0].password);

         if (controlpassword === true) {
           console.log("Contraseña correcta");
           var id = Number.parseInt(body[0].id);
           query = "q=" + JSON.stringify({"id": id});
           console.log("La query es :" + query);
           var loginon = '{"$set":{"logged":true}}';
           httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(loginon),
             function(errPUT, resMLabPUT, bodyPUT) {
               console.log("login realizado");
               var response = {
                 "msg" : "Usuario logado",
                 "id" : body[0].id
               }
               res.send(response);
             }
           )
         }
         else {
               console.log("login incorrecto. Password inválida");
               var response = {
                "mensaje" : "Login incorrecto. Password inválida"
               }
               res.status(401);
               res.send(response);
            }
          }
        else {
          console.log("Usuario no encontrado");

          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
          res.send(response);
         }
        }
      );
     }

     function logoutUsersV1 (req, res){
       console.log("LOGOUT /apitechu/v1/logout/:id");

       var users = require('../usuarios.json');
       var logout = false;
       var usuarioid = 0;

       for (var i = 0; i < users.length; i++) {
          if ((users[i].id == req.params.p1)&& (users[i].logged === true)) {
            usuarioid = users[i].id;
            logout = true;
            delete users[i].logged
            console.log("logout realizado")
              }
            }
             if  (logout == true)
               {var respuesta = {
                   "mensaje":  "logout correcto",
                   "idUsuario": usuarioid
               };
               io.writeUserDataToFile(users);
               console.log("Logout correcto");
               res.send(respuesta);
               }
             else
               {res.send({"mensaje":"logout incorrecto"});
               }
          }

      function logoutUsersV2(req, res) {
        }

module.exports.loginUsersV1 = loginUsersV1;
module.exports.loginUsersV2 = loginUsersV2;
module.exports.logoutUsersV1 = logoutUsersV1;
module.exports.logoutUsersV2 = logoutUsersV2;
